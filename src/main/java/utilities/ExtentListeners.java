package utilities;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class ExtentListeners implements ITestListener{

	public static ExtentTest testreport ;
	
	public void onTestStart(ITestResult result) {
	ExtentTestManager.startTest(result.getMethod().getMethodName());
	}

	public void onTestSuccess(ITestResult result) {
	ExtentTestManager.getTest().log(Status.PASS, "Test Case Passed");
	}

	public void onTestFailure(ITestResult result) {
	ExtentTestManager.getTest().log(Status.FAIL, "Test Case Failed");
	}

	public void onTestSkipped(ITestResult result) {
	ExtentTestManager.getTest().log(Status.SKIP, "Test Case Sipped");
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {}

	public void onStart(ITestContext context) {
	}

	public void onFinish(ITestContext context) {
		ExtentTestManager.endTest();
		ExtentManager.getInstance().flush();
		
	}

}
