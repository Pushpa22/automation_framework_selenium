package utilities;

import java.io.File;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentManager  {

private static	ExtentReports extent;
private static String reportFileName = "Test-Automaton-Report"+".html";
private static String fileSeperator = System.getProperty("file.separator");
private static String reportFilepath = System.getProperty("user.dir") +fileSeperator+ "TestReport";
private static String reportFileLocation =  reportFilepath +fileSeperator+ reportFileName;

public static ExtentReports getInstance() {
    if (extent == null)
        createInstance();
    return extent;
}

public static ExtentReports createInstance() {
	
	 String fileName = getReportPath(reportFilepath);
	ExtentHtmlReporter extenthtmlreporter = new ExtentHtmlReporter(fileName);
	extenthtmlreporter.config().setDocumentTitle("TestExecutionReoprt");
	extenthtmlreporter.config().setTheme(Theme.STANDARD);
	extenthtmlreporter.config().setEncoding("utf-8");
	extenthtmlreporter.config().setReportName(reportFileLocation);
	extenthtmlreporter.config().setReportName(reportFileName);
	extent = new ExtentReports();
	extent.attachReporter(extenthtmlreporter);
	return extent;
}


private static String getReportPath (String path) {
	File testDirectory = new File(path);
    if (!testDirectory.exists()) {
    	if (testDirectory.mkdir()) {
            System.out.println("Directory: " + path + " is created!" );
            return reportFileLocation;
        } else {
            System.out.println("Failed to create directory: " + path);
            return System.getProperty("user.dir");
        }
    } else {
        System.out.println("Directory already exists: " + path);
    }
	return reportFileLocation;
}


	/*
	@AfterTest
	public void endReport() {
		extent.flush();
	}
	
	@AfterMethod
	public void teardown(ITestResult result) {
		
		if(result.getStatus()==ITestResult.FAILURE) {
			
			String methodName= result.getMethod().getMethodName();
			
			String	logText = "<b>"+"TEST CASE" + methodName.toUpperCase()+"    FAILED   " + "</b>";
			
			Markup mup = MarkupHelper.createLabel(logText, ExtentColor.RED);
			
		}
		
		else  if(result.getStatus()==ITestResult.SUCCESS) {
		
			String methodName= result.getMethod().getMethodName();
			
		String	logText = "<b>"+"TEST CASE" + methodName.toUpperCase()+"    PASSED " + "</b>";
		
		Markup mup = MarkupHelper.createLabel(logText, ExtentColor.GREEN);
		
		}
		
		else if(result.getStatus()==ITestResult.SKIP) {
			
		}
	}*/
	
}
