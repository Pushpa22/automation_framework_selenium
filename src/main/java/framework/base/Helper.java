package framework.base;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;

public class Helper {
	
	public static WebDriver driver;
	
	@BeforeMethod(alwaysRun=true)
	public void launchBrowser() throws InterruptedException {
		Helper.driver = ManageDriver.launchChromeBrowser();
		driver.get("http://the-internet.herokuapp.com/");
		Thread.sleep(5000);
	}

	@AfterMethod
	public void close() {
		driver.close();
	}
	
	@AfterSuite
	public void quit() {
		driver.quit();
	}
	
}
