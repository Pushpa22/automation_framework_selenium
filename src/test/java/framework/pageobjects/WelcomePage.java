package framework.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WelcomePage {
	
	WebDriver driver ;
	
	@FindBy(xpath ="//h1[@class='heading']")
	WebElement _textHeading;
	
	@FindBy(xpath ="//a[contains(text(),'Add/Remove Elements')]")
	WebElement _linkAddRemoveElement;
	
	public WelcomePage(WebDriver driver) {
	this.driver = driver;	
	}

	public String getTextOfHeading() {
		return _textHeading.getText();
	}
	
	public void clickOnAddRemoveElement() throws InterruptedException {
		_linkAddRemoveElement.click();
		Thread.sleep(5000);
	}
}
