package framework.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddRemoveElementsPage {
	
WebDriver driver;

@FindBy(xpath="//h3[contains(text(),'Add')]")
WebElement _textAddRemovElement;

@FindBy(xpath="//button[contains(text(),'Add Element')]")
WebElement _buttonAddElement;

public AddRemoveElementsPage(WebDriver driver) {
	this.driver = driver;
}

public String getTextAddRemoveHeading() {
	return _textAddRemovElement.getText();
}
	
}
