package framework.testcases;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import framework.base.Helper;
import framework.pageobjects.AddRemoveElementsPage;
import framework.pageobjects.WelcomePage;

public class ExamplesTest extends Helper{

	@Test
	public void testAddRemoveElements() throws InterruptedException {
		
		WelcomePage welcomepage = PageFactory.initElements(driver, WelcomePage.class);
		AddRemoveElementsPage addremoveelementspage = PageFactory.initElements(driver, AddRemoveElementsPage.class);
		
		welcomepage.clickOnAddRemoveElement();
		Assert.assertTrue(addremoveelementspage.getTextAddRemoveHeading().contains("Add/Remove Elements"));
	}
	
}
